#!/bin/bash

function log() {
    echo -e "\n$(date '+%Y-%m-%d %H:%M:%S') $1"
}

function check() {
    if [ ! -f /usr/local/bin/gitlab-runner ]; then
        log "gitlab-runner not found!"
        sudo wget -q -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
        log "gitlab-runner downloaded!"
        sudo chmod +x /usr/local/bin/gitlab-runner
        log "gitlab-runner set executable!"
        sudo gitlab-runner install --user=root --working-directory=/root
        log "gitlab-runner installed!"
        sudo gitlab-runner start
        log "gitlab-runner started!"
    fi
}

function register() {
    CI_SERVER_URL="http://gitlab.example.com/"
    REGISTRATION_TOKEN="1111111aaaaaaaaa"
    RUNNER_TAG_LIST="gitlab"
    DOCKER_IMAGE="docker:19.03.5"
    sudo gitlab-runner register --non-interactive --executor docker --url $CI_SERVER_URL --registration-token $REGISTRATION_TOKEN --description $HOSTNAME --tag-list $RUNNER_TAG_LIST --docker-image $DOCKER_IMAGE --docker-volumes /var/run/docker.sock:/var/run/docker.sock
}

check
register
