#!/bin/sh

function log() {
    echo -e "\n$(date '+%Y-%m-%d %H:%M:%S') $1"
}

function check() {
    if [ -z "$GITLAB_ACCESS_TOKEN" ]; then
        log "GitLab Acccess Token parameter is required!"
        exit 1
    fi

    if [ ! -f $ROOT_PATH/permissions.csv ]; then
        log "permissions.csv file not found!"
        exit 1
    fi
}

function createUsers() {
    while read USER; do
        IS_ADMIN=$(echo $USER | cut -d, -f1)
        USERNAME=$(echo $USER | cut -d, -f2)
        NAME=$(echo $USER | cut -d, -f3)
        EMAIL=$(echo $USER | cut -d, -f4)
        PASSWORD=$(echo $USER | cut -d, -f5)
        PERMISSIONS=$(echo $USER | cut -d, -f6)

        if [ -z "$IS_ADMIN" ] || [ -z "$USERNAME" ] || [ -z "$NAME" ] || [ -z "$EMAIL" ] || [ -z "$PERMISSIONS" ]; then
            log "$USER line has missing info! Skipped!"
        else
            USER_ID=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/users?username=$USERNAME" | jq ".[] | .id")
            if [ -z "$USER_ID" ]; then
                log "User $USERNAME not found! Creating new user!"
                curl -fs -XPOST -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" -d "name=$NAME&email=$EMAIL&username=$USERNAME&password=${PASSWORD:-is-empty}&admin=$IS_ADMIN&skip_confirmation=true" "$GITLAB_URL/users"
                USER_ID=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/users?username=$USERNAME" | jq ".[] | .id")
            fi

            for PERMISSION in $PERMISSIONS; do
                GROUP=$(echo $PERMISSION | cut -d: -f1)
                LEVEL=$(echo $PERMISSION | cut -d: -f2)

                GROUP_ID=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/groups" | jq ".[] | select (.name==\"$GROUP\") | .id")
                if [ -z "$GROUP_ID" ]; then
                    log "Group $GROUP not found! Creating new group!"
                    curl -fs -XPOST -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" -d "name=$GROUP&path=$GROUP" "$GITLAB_URL/groups"
                    GROUP_ID=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/groups" | jq ".[] | select (.name==\"$GROUP\") | .id")
                fi

                ACCESS_LEVEL=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/groups/$GROUP_ID/members/$USER_ID" | jq ".access_level")
                if [ -z "$ACCESS_LEVEL" ]; then
                    log "User $USER_ID is not member of group $GROUP ! Adding user to group!"
                    curl -fs -XPOST -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" -d "user_id=$USER_ID&access_level=$LEVEL" "$GITLAB_URL/groups/$GROUP_ID/members"
                elif [ "$LEVEL" != "$ACCESS_LEVEL" ]; then
                    log "User $USER_ID access level for group $GROUP, is setting $ACCESS_LEVEL to $LEVEL !"
                    curl -fs -XPUT -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/groups/$GROUP_ID/members/$USER_ID?access_level=$LEVEL"
                fi
            done
        fi
    done < $ROOT_PATH/permissions.csv
}

function integrateJira() {
    USERNAME=jira.service.user
    PASSWORD=j1ra.5erv1ce.pa$$
    WEB_URL=http://jira.example.com
    API_URL=http://jira.example.com/rest/api/2

    CONTENT=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/projects?per_page=500&simple=true")
    IFS=$'\n'
    for PROJECT_ID in $(echo $CONTENT | jq ".[] | .id"); do
        curl -fs -XPUT -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" -d "url=$WEB_URL&api_url=$API_URL&username=$USERNAME&password=$PASSWORD&active=true" "$GITLAB_URL/projects/$PROJECT_ID/services/jira"
    done
}

function integrateSlack() {
    WEBHOOK=https://hooks.slack.com/services/XXXXXXXX/1111111111/111111111XXXXXXXXXXXX
    USERNAME=GitLab
    NOTIFY_ONLY_BROKEN_PIPELINES=true
    NOTIFY_ONLY_DEFAULT_BRANCH=true
    BRANCHES_TO_BE_NOTIFIED=default
    PUSH_EVENTS=true
    ISSUES_EVENTS=false
    CONFIDENTIAL_ISSUES_EVENTS=false
    MERGE_REQUESTS_EVENTS=true
    NOTE_EVENTS=false
    CONFIDENTIAL_NOTE_EVENTS=false
    TAG_PUSH_EVENTS=false
    PIPELINE_EVENTS=true
    WIKI_PAGE_EVENTS=false
    DEPLOYMENT_EVENTS=false
    CHANNEL=dev-team

    CONTENT=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/projects?per_page=500&simple=true")
    IFS=$'\n'
    for PROJECT_ID in $(echo $CONTENT | jq ".[] | .id"); do
        curl -fs -XPUT -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" -d "webhook=$WEBHOOK&username=$USERNAME&channel=$CHANNEL&notify_only_broken_pipelines=$NOTIFY_ONLY_BROKEN_PIPELINES&notify_only_default_branch=$NOTIFY_ONLY_DEFAULT_BRANCH&branches_to_be_notified=$BRANCHES_TO_BE_NOTIFIED&push_events=$PUSH_EVENTS&push_channel=$CHANNEL&issues_events=$ISSUES_EVENTS&issue_channel=$CHANNEL&confidential_issues_events=$CONFIDENTIAL_ISSUES_EVENTS&confidential_issue_channel=$CHANNEL&merge_requests_events=$MERGE_REQUESTS_EVENTS&merge_request_channel=$CHANNEL&note_events=$NOTE_EVENTS&note_channel=$CHANNEL&confidential_note_events=$CONFIDENTIAL_NOTE_EVENTS&confidential_note_channel=$CHANNEL&tag_push_events=$TAG_PUSH_EVENTS&tag_push_channel=$CHANNEL&pipeline_events=$PIPELINE_EVENTS&pipeline_channel=$CHANNEL&wiki_page_events=$WIKI_PAGE_EVENTS&wiki_page_channel=$CHANNEL&deployment_events=$DEPLOYMENT_EVENTS&deployment_channel=$CHANNEL" "$GITLAB_URL/projects/$PROJECT_ID/services/slack"
    done
}

function blockDeletedUsers() {
    CONTENT=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/users?active=true")
    for USERNAME in $(echo $CONTENT | jq ".[] | .username" | sed 's/"//g'); do
        if [ "$USERNAME" != "root" ]; then
            if ! grep -q ",$USERNAME," $ROOT_PATH/permissions.csv; then
                USER_ID=$(echo $CONTENT | jq ".[] | select (.username==\"$USERNAME\") | .id")
                log "$USERNAME ($USER_ID) will be blocked!"
                curl -fs -XPOST -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/users/$USER_ID/block"
            fi
        fi
    done
}

function unblockAddedUsers() {
    CONTENT=$(curl -fs -XGET -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/users?blocked=true")
    for USERNAME in $(echo $CONTENT | jq ".[] | .username" | sed 's/"//g'); do
        if grep -q ",$USERNAME," $ROOT_PATH/permissions.csv; then
            USER_ID=$(echo $CONTENT | jq ".[] | select (.username==\"$USERNAME\") | .id")
            log "$USERNAME ($USER_ID) will be unblocked!"
            curl -fs -XPOST -H "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_URL/users/$USER_ID/unblock"
        fi
    done
}

IFS=' '
ROOT_PATH=$(dirname "$0")

GITLAB_ACCESS_TOKEN=111-aaaaaaaaaaaa
GITLAB_URL=http://gitlab.example.com/api/v4

check
createUsers
integrateJira
integrateSlack
blockDeletedUsers
unblockAddedUsers
